package main

import (
	"context"
	"flag"
	"net/http"
	"os"

	"github.com/golang/glog"
	"github.com/grpc-ecosystem/grpc-gateway/runtime"
	"google.golang.org/grpc"

	pb "gitlab.com/demo-micro-services/protobuf/middleware/demo"
)

var (
	midDemoEndpoint = "mid-demo:50050"
)

func run() error {
	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	// Register gRPC server endpoint
	// Note: Make sure the gRPC server is running properly and accessible
	mux := runtime.NewServeMux()
	opts := []grpc.DialOption{grpc.WithInsecure()}
	err := pb.RegisterDemoAPIServiceHandlerFromEndpoint(ctx, mux, midDemoEndpoint, opts)
	if err != nil {
		return err
	}
	glog.Infoln("api-gateway start 8080")
	// Start HTTP server (and proxy calls to gRPC server endpoint)
	return http.ListenAndServe(":8080", mux)
}

func main() {
	os.Args = append(os.Args, "-logtostderr=true")
	flag.Parse()

	if err := run(); err != nil {
		glog.Fatal(err)
	}
}
